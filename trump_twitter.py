#!/usr/bin/env python
#encoding=utf-8

import tweepy
import pandas as pd
import re
import logging
from textblob import TextBlob
from sqlaalchemy import create_engine

CONSUMER_KEY = "TTJ0dSy7Imm8HxvgvNwjA"
CONSUMER_SECRET = "yiYwrvyDmidzmbupHc2xex9jt11twVyNPjlf0gkIA"
ACCESS_TOKEN = "346440831-eiA5APw2Y16XflPloiCj7cpleaODzfM7L0CI5LSw"
ACCESS_TOKEN_SECRET = "ynFkbgO63mmjVXVZ81EdUEhiUiVxvaRj72HPUzrxftA"

class trump_twitter():

    def get_trump_info(self):
        auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
        auth.set_access_token(ACCESS_TOKEN, ACCESS_TOKEN_SECRET)
        api = tweepy.API(auth)

        screen_name = '@realDonaldTrump'
        tweets = api.user_timeline(screen_name, count=10)
        data = []
        for tweet in tweets:
            data.append([tweet.id, tweet.text, len(tweet.text), analize_sentiment(clean_tweet(tweet.text)), tweet.source, \
            tweet.favorite_count, tweet.retweet_count, tweet.created_at])
	
        return pd.DataFrame(data=data, columns=['id', 'text', 'length', 'sentiment', 'source', 'favorite', 'retweet', 'create_time'])
    
    def clean_tweet(tweet):
    	'''
    	Utility function to clean the text in a tweet by removing 
    	links and special characters using regex.
    	'''
    	return ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)", " ", tweet).split())

    def analize_sentiment(tweet):
    	'''
    	Utility function to classify the polarity of a tweet
    	using textblob.
    	'''
    	analysis = TextBlob(clean_tweet(tweet))
    	if analysis.sentiment.polarity > 0:
            return 1
    	elif analysis.sentiment.polarity == 0:
            return 0
    	else:
            return -1        

if __name__ == '__main__':
    trump = trump_twitter()
    df = trump.get_trump_info()
    engine = create_engine("mysql://root:wealth9021@115.28.185.117/invest")
    trump_twitter_id = pd.read_sql_query("select id from trump_twitter limit 1", con=engine)
    if trump_twitter_id:
        df.to_sql(name='trump_twitter', con=engine, if_exists='append')
        logging.info("Init table trump_twitter, insert {} lines".format(df.shape[0]))
    else:
        df_insert = df[df['id']>trump_twitter_id]
        df_insert.to_sql(name='trump_twitter', con=engine, if_exists='append')
        logging.info("Insert {} lines to trump_twitter successfully.".format(df_insert.shape[0])) 
